
[[ -s "$HOME/.profile" ]] && source "$HOME/.profile" # Load the default .profile

if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

export PATH="/home/tsv/.ebcli-virtual-env/executables/:/home/tsv/.rbenv/bin/:$PATH"
eval "$(rbenv init -)"

