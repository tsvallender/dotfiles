(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("/home/tsv/notes/fs_todo.org" "/home/tsv/notes/today.org" "/home/tsv/notes/tasks.org" "/home/tsv/notes/habits.org" "/home/tsv/notes/todo.org"))
 '(package-selected-packages
   '(all-the-icons-dired visual-fill-column dashboard no-littering org-recur shell-pop org-superstar org-superstar-mode org use-package rainbow-delimiters org-bullets hydra doom-themes doom-modeline))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t (:family "Cascadia Code" :height 1.1 :weight normal))))
 '(variable-pitch ((t (:family "CMU Concrete" :height 1.3 :weight normal)))))
