source ~/.profile
source ~/.zsh_git.sh
source ~/.zsh_prompt.sh

# Commands without arguments should resume proc if possible
setopt auto_resume
# Don't overwrite history file
setopt append_history
# Turn off flow control
setopt no_flow_control

# Emacs-style bindings
bindkey -e

export GOPATH=$HOME/code/go
export EDITOR='emacsclient -n'
export PAGER='less'

alias e=$EDITOR
alias ls='ls --color'
alias ll='ls -l'
alias la='ls -lA'
alias osync='osync.sh'

alias dockerprune='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker system prune --force && docker volume rm $(docker volume ls -qf dangling=true)'

export PATH="$PATH:$HOME/.local/share/flatpak/app:$HOME/.rvm/bin:$HOME/bin:$HOME/code/adr-tools/src"
#
#
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This

source ~/.dotfiles/secrets

