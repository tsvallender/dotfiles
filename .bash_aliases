# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias checkport='lsof -i'

# Get rid of everything docker
alias dockerpurge='docker images -a -q | xargs docker rmi -f'
alias dockerprune='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker system prune --force && docker volume rm $(docker volume ls -qf dangling=true)'

alias dip='RBENV_VERSION=3.1.1 dip'

